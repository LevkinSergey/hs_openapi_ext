﻿// Обслуживание подсистемы
//  
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// При создании административной формы
//
// Параметры: 
// 	Форма - УправляемаяФорма - Создаваемая форма
// 	Отказ - Булево - Флаг отказа создания
// 	СтандартнаяОбработка - Булево - Флаг стандартной обработки
//
Процедура ФормаСлужебнаяПриСозданииНаСервере(Форма, Отказ, СтандартнаяОбработка) Экспорт
	
	Отказ	= Истина;
	
	Сообщение	= Новый СообщениеПользователю;
	Сообщение.Текст = НСтр("ru = 'Это служебный объект, интерактивное использование запрещено.'");
	Сообщение.Сообщить(); 
	
КонецПроцедуры // ФормаСлужебнаяПриСозданииНаСервере 

#КонецОбласти


#Область СлужебныйПрограммныйИнтерфейс

// Инициализация подсистемы
//
// Параметры: 
//
Процедура Инициализировать() Экспорт
	
	оас_СправкаАдаптер.Инициализировать();
	
КонецПроцедуры // Инициализировать 

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on
 